JabRef plugin to massively rename attached files
================================================

This project is a plugin for BibTeX editor [JabRef][1]. This editor can attach
a number of file links with each BibTeX record which is very convenient, but
keeping file names consistent to entry metadata may be very tedious. This plugin
can be used to rename files attached to entries to reflect metadata. Naming
pattern uses an extended syntax of the [BibTeX key generator][2] of JabRef.
For more infromation and examples see the help page of the plugin.

[1]: http://jabref.sourceforge.net/
[2]: http://jabref.sourceforge.net/help/LabelPatterns.php

Installation
------------

1. Download the JAR file for the plugin.
2. Go to _Plugins — Manage plugins — Install plugin_ in JabRef interface.
3. Specify path to downloaded JAR file.
4. Restart JabRef.

Authors
-------

This work is based on original plugin “renamefile” created by Sergey Kor which
can be found on [Github][3]. The work in provided under the terms of MIT license,
see LICENSE.txt file for more information.

[3]: https://github.com/korv/Jabref-plugins/ 

