package org.rvlm.jabref.renamefiles2;

import java.util.ArrayList;
import java.util.Objects;
import net.sf.jabref.BibtexDatabase;
import net.sf.jabref.BibtexEntry;
import net.sf.jabref.BibtexEntryType;
import net.sf.jabref.Globals;
import net.sf.jabref.gui.FileListEntry;
import net.sf.jabref.labelPattern.LabelPatternUtil;

public final class PatternUtil {

	public static void setDatabase(BibtexDatabase db) {
		Objects.requireNonNull(db);
		LabelPatternUtil.setDataBase(db);
	}
	
	public static String generateFileName(
			BibtexEntry entry,
			FileListEntry fileEntry,
			NameGenerationOptions options) {
		
		Objects.requireNonNull(entry);
		Objects.requireNonNull(fileEntry);
		Objects.requireNonNull(options);
		Objects.requireNonNull(options.targetDirectory);
		Objects.requireNonNull(options.targetPattern);
		
		String sourceFileLink  = fileEntry.getLink();
		String sourceDirectory = FileNameUtils.getDirectory(sourceFileLink);
		String sourceFileName  = FileNameUtils.getBaseName(sourceFileLink);
		String sourceFileExt   = FileNameUtils.getExtension(sourceFileName);
		
		if (options.keepSameDirectory && options.keepSameBaseName)
			return FileNameUtils.fixSlashes(sourceFileLink);
		
		if (options.keepSameDirectory && !options.keepSameBaseName) {
			return FileNameUtils.joinPaths(
				sourceDirectory,
				FileNameUtils.appendExtension(
					makeLabel(options.targetPattern, entry, fileEntry),
					sourceFileExt));
		}
		
		if (!options.keepSameDirectory && options.keepSameBaseName) {
			return FileNameUtils.joinPaths(
				options.targetDirectory,
				sourceFileName);
		}
		
		if (!options.keepSameDirectory && !options.keepSameBaseName) {
			return FileNameUtils.joinPaths(
				options.targetDirectory,
				FileNameUtils.appendExtension(
					makeLabel(options.targetPattern, entry, fileEntry),
					sourceFileExt));
		}
		
		// TODO: More appropriate exception type?
		throw new AssertionError("Unreachable code");
	}

	public static String makeLabel(
			String pattern,
			BibtexEntry entry,
			FileListEntry fileEntry) {

		final String[] regexesToSubstitute = {
				"\\\\([oilL])[\\s\\}]+",
				"\\\\a(a)[\\s\\}]+",
				"\\\\A(A)[\\s\\}]+",
				"\\\\(ss)[\\s\\}]+",               // symbols
				"\\\\[a-zA-Z]+[\\s\\{\\}\\\\]+()", // latex commands
				"\\\\[^\\w][\\s\\{]*([a-zA-z])" }; // accents
		
		final String[] regexesToRemove = { 
				"\\\\", "/", "<", ">", "\\?", "\\{", "\\}",
				"\\$", "\"", "\n", ":" };
		
		StringBuffer result = new StringBuffer();
		boolean insideFieldMarker = false;
		
		// Unfortunately, LabelPatternUtil.split emits whole source
		// pattern as tokens[0] which we have to skip when traversing
		// in loop. This makes foreach loop syntax here not applicable.
		ArrayList<String> tokens = LabelPatternUtil.split(pattern);
		for (int i = 1; i < tokens.size(); i++) {
			String token = tokens.get(i);
			if (token.equals("[")) {
				insideFieldMarker = true;
				continue;
			}
			
			if (token.equals("]")) {
				insideFieldMarker = false;
				continue;
			}
			
			if (!insideFieldMarker)
				result.append(token);
			else {
				String[] parts = LabelPatternUtil.parseFieldMarker(token);
				if (parts == null || parts.length == 0)
					continue;
				
				String name = parts[0];
				String value = null;
				
				if (name.equals("type")) {
					BibtexEntryType entryType = entry.getType();
					value = entryType != null ? entryType.getName() : "";
				} else if (name.equals("description")) {
					value = fileEntry != null ? fileEntry.getDescription() : "";
				} else {
					value = LabelPatternUtil.makeLabel(entry, name);
				}
				
				value = applyModifiers(value, parts);
				
				for (String regex: regexesToSubstitute)
					value = value.replaceAll(regex, "$1");
				
				for (String regex: regexesToRemove)
					value = value.replaceAll(regex, "");
				
				result.append(value);
			}
		}

		return result.toString();
	}

	// TODO: Make better refactoring.
	private static String applyModifiers(String label, String[] parts) {
		for (int j = 1; j < parts.length; j++) {
			String modifier = parts[j];
			if (modifier.equals("lower")) {
				label = label.toLowerCase();
			} else if (modifier.equals("upper")) {
				label = label.toUpperCase();
			} else if (modifier.equals("abbr")) {
				StringBuffer abbr = new StringBuffer();
				String[] words = label.toString().replaceAll("[\\{\\}']", "")
						.split("[\\(\\) \r\n\"]");
				for (String word : words)
					if (!word.isEmpty())
						abbr.append(word.charAt(0));
				label = abbr.toString();
			} else if (modifier.equals("regex") && j + 2 < parts.length) {
				label = label.replaceAll(parts[j + 1], parts[j + 2]);
				j = j + 2;
			} else if (modifier.startsWith("(") && modifier.endsWith(")")) {
				// Alternate text modifier in parentheses. Should be inserted if
				// the label is empty:
				if (label.equals("") && (modifier.length() > 2))
					label = modifier.substring(1, modifier.length() - 1);
			} else if (modifier.startsWith("!(") && modifier.endsWith(")")) {
				// Alternate text modifier in parentheses. Should be inserted
				// unless
				// the label is empty:
				if (!label.equals("") && (modifier.length() > 2))
					label = modifier.substring(2, modifier.length() - 1);
			} else {
				Globals.logger("File name generator warning: unknown modifier '"
						+ modifier + "'.");
			}
		}
		return label;
	}
}
