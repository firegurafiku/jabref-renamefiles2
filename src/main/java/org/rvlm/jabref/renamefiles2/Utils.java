package org.rvlm.jabref.renamefiles2;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Objects;
import java.util.Vector;
import net.sf.jabref.BibtexEntry;
import net.sf.jabref.Globals;
import net.sf.jabref.MetaData;
import net.sf.jabref.gui.FileListEntry;
import net.sf.jabref.gui.FileListTableModel;

public final class Utils {

	public static FileListEntry getFileEntry(BibtexEntry b, String fn) {
		FileListTableModel m = new FileListTableModel();
		m.setContent(b.getField("file"));
		for (int j = 0; j < m.getRowCount(); j++) {
			FileListEntry f = m.getEntry(j);
			if (f.getLink().equals(fn))
				return f;
		}

		return null;
	}

	// TODO: Make better refactoring.
	// TODO: ... or do not make? It seems code was borrowed from JabRef.
	// code from Metadata.getFileDirectory
	public static String getDatabaseDirectoryForFiles(MetaData metadata) {
		Objects.requireNonNull(metadata);
		
		File file = metadata.getFile(); // bib file
		
		// Check if the bib file location is set and primary:
		if (file != null
			&& Globals.prefs.getBoolean("bibLocationAsFileDir")
			&& Globals.prefs.getBoolean("bibLocAsPrimaryDir"))
			return file.getParent();
		
		String dir;
		String key = Globals.prefs.get("userFileDirIndividual");
		Vector<String> vec = metadata.getData(key);
		if (vec == null) {
			key = Globals.prefs.get("userFileDir");
			vec = metadata.getData(key);
		}
		
		if (vec == null)
			vec = metadata.getData("fileDirectory"); // 2.6?
		
		if (vec != null && vec.size() > 0)
			dir = vec.get(0);
		else {
			// This is a global file directory
			// In the original code it is called by get(field+"Directory")
			dir = Globals.prefs.get("fileDirectory");
		}
		
		if (dir == null)
			dir = "";
		
		// If this directory is relative, we try to interpret it as relative to
		// the file path of this bib file:
		if (file != null && !new File(dir).isAbsolute()) {
			String relDir;
			if (dir.equals(".")) {
				relDir = file.getParent();
			} else {
				String separator = System.getProperty("file.separator");
				relDir = file.getParent() + separator + dir;
			}
			
			// If this directory actually exists, it is very likely that the
			// user wants us to use it:
			if (new File(relDir).exists())
				dir = relDir;
		}
		
		return dir;
	}

	public static void updateBibtexEntryFile(
			BibtexEntry bibtexEntry, FileListEntry fileEntry, String fileLink) {
		
		String fileField = bibtexEntry.getField("file");
		if (fileField == null || fileField == "")
			return;
		
		FileListTableModel model = new FileListTableModel();
		model.setContent(fileField);
		
		for (int i = 0; i < model.getRowCount(); i++) {
			FileListEntry entry = model.getEntry(i);
			if (entry.getType().compareTo(fileEntry.getType()) != 0 ||
			    !entry.getLink().equals(fileEntry.getLink()) ||
			    !entry.getDescription().equals(fileEntry.getDescription()))
				continue;
			
			entry.setType(fileEntry.getType());
			entry.setLink(fileLink);
			entry.setDescription(entry.getDescription());
			break;
		}

		bibtexEntry.setField("file", model.getStringRepresentation());
	}
	
	// TODO: @christina Google for better file copy routine.
	public static boolean copyFile(File of, File nf) {
		// TODO: OMFG, no better way to copy files in Java? 
		InputStream in;
		try {
			in = new FileInputStream(of);
			OutputStream out = new FileOutputStream(nf);
			byte[] buf = new byte[1024];
			int len;
			while ((len = in.read(buf)) > 0) {
				out.write(buf, 0, len);
			}
			in.close();
			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return false;
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
}
