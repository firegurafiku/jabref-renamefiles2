package org.rvlm.jabref.renamefiles2;
import java.io.File;
import java.io.IOException;
import java.util.Objects;

public final class FileNameUtils {
	
	public static String getExtension(String fileName) {
		Objects.requireNonNull(fileName);
		int dotIndex = fileName.lastIndexOf('.');
		return dotIndex >= 0 ? fileName.substring(dotIndex + 1) : "";
	}

	public static boolean haveSameExtension(
			String fileName1,
			String fileName2) {
		
		Objects.requireNonNull(fileName1);
		Objects.requireNonNull(fileName2);
		
		String extension1 = getExtension(fileName1);
		String extension2 = getExtension(fileName2);
		return extension1 == null
			? extension2 == null
			: extension1.equalsIgnoreCase(extension2);
	}

	public static String removeExtension(String path) {
		Objects.requireNonNull(path);
		int dotIndex = path.lastIndexOf('.');
		return dotIndex >= 0 ? path.substring(0, dotIndex) : path;
	}
	
	public static String appendExtension(String path, String ext) {
		Objects.requireNonNull(path);
		Objects.requireNonNull(ext);
		return ext.equals("") ? path : path + "." + ext; 
	}

	
	public static String getDirectory(String fileName) {
		Objects.requireNonNull(fileName);
		fileName = fixSlashes(fileName);
		int slashIndex = fileName.lastIndexOf(File.separatorChar);
		return slashIndex >= 0
			? fileName.substring(0, slashIndex)
			: "";
	}
	
	public static String getBaseName(String fileName) {
		Objects.requireNonNull(fileName);
		fileName = fixSlashes(fileName);
		int slashIndex = fileName.lastIndexOf(File.separatorChar);
		return slashIndex >= 0
			? fileName.substring(slashIndex + 1)
			: fileName;
	}
	
	public static String getFileNameVariant(String fileName, int variant) {
		return FileNameUtils.joinPaths(
			FileNameUtils.getDirectory(fileName),
			FileNameUtils.appendExtension(
				FileNameUtils.getBaseName(fileName) + variant,
				FileNameUtils.getExtension(fileName)));
	}
	
	public static boolean isAbsolute(String path) {
		Objects.requireNonNull(path);
		return fixSlashes(path).startsWith(File.separator);
	}
	
	public static String joinPaths(String path1, String path2) {
		Objects.requireNonNull(path1);
		Objects.requireNonNull(path2);
		return fixSlashes(path1 + File.separator + path2);
	}
	
	public static String fixSlashes(String path) {
		return path.replaceAll("[\\\\/]+", File.separator);
	}

	public static boolean areSameFiles(String oldFileName, String newFileName) {
		try {
			File oldFile = new File(oldFileName);
			File newFile = new File(newFileName);
			return oldFile.getCanonicalPath().equals(newFile.getCanonicalPath());
		} catch (IOException e) {
			return false;
		}
	}

}
