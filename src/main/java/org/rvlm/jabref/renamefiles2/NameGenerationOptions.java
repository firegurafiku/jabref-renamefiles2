package org.rvlm.jabref.renamefiles2;

public final class NameGenerationOptions {

	public boolean keepSameDirectory;
	
	public boolean keepSameBaseName;
	
	public String targetDirectory;
	
	public String targetPattern;
}
