package org.rvlm.jabref.renamefiles2;

import javax.swing.*;
import javax.swing.table.*;
import org.dyno.visual.swing.layouts.Bilateral;
import org.dyno.visual.swing.layouts.Constraints;
import org.dyno.visual.swing.layouts.GroupLayout;
import org.dyno.visual.swing.layouts.Leading;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.Objects;
import net.sf.jabref.*;
import net.sf.jabref.gui.*;

// TODO: @christina Make not implement ActionListener.
public final class RenameDialog extends JDialog implements ActionListener,
		FocusListener {
	private static final long serialVersionUID = 1L;
	private final int[] renWidth = { 10, 100, 100, 300, 300, 300 };
	private final int[] delWidth = { 10, 100, 100, 300, 300 };
	private JabRefFrame frame;
	private BibtexEntry[] bes;
	private int mode;

	private FLTableModel tm;
	private JTable table;
	private JPanel panel;
	private JCheckBox cbName, cbDir;
	private JTextField tfDir, tfPattern;
	private JButton btnOk, btnCancel;

	private final String[] titleString = { "Rename/move files", "Copy files",
			"Delete files" };
	
	private MetaData metadata;
	private String baseDirectory;
	
	public RenameDialog(JabRefFrame frame, BibtexEntry[] bes, MetaData metadata, String baseDirectory) {
		this(frame, bes, metadata, baseDirectory, 0);
	}

	public RenameDialog(JabRefFrame frame, BibtexEntry[] bes, MetaData metadata, String baseDirectory, int mode) {
		super(frame);
		this.metadata = metadata;
		this.baseDirectory = baseDirectory;
		this.frame = frame;
		this.bes = bes;
		this.mode = mode;
		initElements();

		JPanel p, p1;
		panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		add(panel);

		p = new JPanel();
		p.setLayout(new BoxLayout(p, BoxLayout.PAGE_AXIS));
		p.add(new JScrollPane(table));
		p.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		panel.add(p);

		if (mode != 2) {
			p = new JPanel();
			p.setLayout(new GroupLayout());
			p.add(cbDir, new Constraints(new Leading(10, 150, 8, 8),
					new Leading(10, 10, 10)));
			p.add(cbName, new Constraints(new Leading(10, 150, 8, 8),
					new Leading(38, 8, 8)));
			p.add(tfDir, new Constraints(new Bilateral(170, 12, 4),
					new Leading(12, 12, 12)));
			p.add(tfPattern, new Constraints(new Bilateral(170, 12, 4),
					new Leading(40, 12, 12)));

			p1 = new JPanel();
			p1.setLayout(new BoxLayout(p1, BoxLayout.X_AXIS));
			p1.add(p);
			panel.add(p1);
		}

		p = new JPanel();
		p.add(btnOk);
		p.add(btnCancel);
		p1 = new JPanel();
		p1.setLayout(new BoxLayout(p1, BoxLayout.X_AXIS));
		p1.add(p);
		panel.add(p1);

		setTitle(titleString[mode]);
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		getRootPane().setDefaultButton(btnOk);
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
	}

	private void initElements() {
		if (mode != 2) {
			cbDir = new JCheckBox();
			cbDir.setText("Use folder:");
			cbDir.addActionListener(this);
			cbName = new JCheckBox();
			cbName.setText("Use name pattern:");
			cbName.addActionListener(this);
			tfDir = new JTextField();
			tfDir.addFocusListener(this);
			tfDir.addActionListener(this);
			tfPattern = new JTextField();
			tfPattern.addFocusListener(this);
			tfPattern.addActionListener(this);
			readValues();
		}
		btnOk = new JButton();
		btnOk.setText("Ok");
		btnOk.addActionListener(this);
		btnOk.setPreferredSize(new Dimension(100, 26));
		btnCancel = new JButton();
		btnCancel.setText("Cancel");
		btnCancel.addActionListener(this);
		btnCancel.setPreferredSize(new Dimension(100, 26));
		tm = new FLTableModel(bes, mode);
		table = new JTable(tm);
		table.setPreferredScrollableViewportSize(new Dimension(800, 200));
		table.setFillsViewportHeight(true);
		// table.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);
		int[] width = mode == 2 ? delWidth : renWidth;
		for (int i = 0; i < width.length; i++)
			table.getColumnModel().getColumn(i).setPreferredWidth(width[i]);
	}

	class FLTableModel extends AbstractTableModel {
		private static final long serialVersionUID = 1L;
		private final String[] renNames = { "", "Key", "Author", "Title",
				"File", "New File" };
		private final String[] delNames = { "", "Key", "Author", "Title",
				"File" };
		private String[] columnNames;
		public ArrayList<HashMap<Object, Object>> data;
		private int mode;

		public FLTableModel(BibtexEntry[] b, int mode) {
			this.mode = mode;
			columnNames = mode == 2 ? delNames : renNames;
			setData(b, getNameGenerationOptions());
		}

		public void setData(
				BibtexEntry[] entries,
				NameGenerationOptions options) {
			
			data = new ArrayList<HashMap<Object, Object>>();
			for (BibtexEntry entry: entries) {
				String fileField = entry.getField("file");
				if (fileField == null || fileField == "")
					continue;
				
				FileListTableModel model = new FileListTableModel();
				model.setContent(fileField);
				for (int i = 0; i < model.getRowCount(); i++) {
					FileListEntry fileEntry = model.getEntry(i);
					
					HashMap<Object, Object> r = new HashMap<Object, Object>();
					r.put(0, true);
					r.put(1, tostr(entry.getField(BibtexFields.KEY_FIELD)));
					r.put(2, tostr(entry.getField("author")));
					r.put(3, tostr(entry.getField("title")));
					r.put(4, fileEntry.getLink());
					r.put("fe", fileEntry);
					r.put("be", entry);
					if (mode != 2)
						r.put(5, getNewFileName(entry, fileEntry, options));
					
					data.add(r);
				}
			}
		}

		private Object tostr(Object s) {
			return s == null ? "" : s;
		}

		public void changeData(BibtexEntry[] b) {
			setData(b, getNameGenerationOptions());
			fireTableDataChanged();
		}

		public int getColumnCount() {
			return columnNames.length;
		}

		public int getRowCount() {
			return data.size();
		}

		public String getColumnName(int col) {
			return columnNames[col];
		}

		public Object getValueAt(int row, int col) {
			return data.get(row).get(col);
		}

		public Class<? extends Object> getColumnClass(int c) {
			return getValueAt(0, c).getClass();
		}

		public boolean isCellEditable(int row, int col) {
			if (col < 1) {
				return true;
			} else {
				return false;
			}
		}

		public void setValueAt(Object value, int row, int col) {
			data.get(row).put(col, value);
			fireTableCellUpdated(row, col);
		}

		public HashMap<Object, Object> getRow(int row) {
			return data.get(row);
		}
	}

	private void readValues() {
		cbDir.setSelected(!Globals.prefs.getBoolean("SameFolder"));
		cbName.setSelected(!Globals.prefs.getBoolean("SameName"));
		tfDir.setText(Globals.prefs.get("MoveFolder"));
		tfPattern.setText(Globals.prefs.get("RenamePattern"));
	}

	private void saveValues() {
		Globals.prefs.putBoolean("SameFolder", !cbDir.isSelected());
		Globals.prefs.putBoolean("SameName", !cbName.isSelected());
		Globals.prefs.put("MoveFolder", tfDir.getText());
		Globals.prefs.put("RenamePattern", tfPattern.getText());
	}

	public void actionPerformed(ActionEvent evt) {
		Object source = evt.getSource();
		
		if (source == btnOk) {
			if (mode != 2)
				saveValues();
			
			processFileOperations(mode);
			
			dispose();
			if (mode != 1) {
				frame.basePanel().markBaseChanged();
				frame.basePanel().updateEntryEditorIfShowing();
			}
			return;
		}
		
		if (source == btnCancel) {
			dispose();
			return;
		}
		
		if (source == cbDir  ||
			source == cbName ||
			source == tfDir  ||
			source == tfPattern) {
			if (mode != 2)
				tm.changeData(bes);
			return;
		}
	}

	public void focusLost(FocusEvent e) {
		tm.changeData(bes);
	}

	public NameGenerationOptions getNameGenerationOptions() {
		NameGenerationOptions options = new NameGenerationOptions();
		options.keepSameDirectory = !cbDir.isSelected();
		options.keepSameBaseName  = !cbName.isSelected();
		options.targetDirectory   = tfDir.getText();
		options.targetPattern     = tfPattern.getText();

		return options;
	}
	
	public String getNewFileName(
			BibtexEntry entry,
			FileListEntry fileEntry,
			NameGenerationOptions options) {
		
		return PatternUtil.generateFileName(entry, fileEntry, options);
	}

	private void processFileOperations(int mode) {
		for (int i = 0; i < tm.getRowCount(); i++) {
			HashMap<Object, Object> r = tm.getRow(i);
			
			boolean isSelected = (Boolean)r.get(0);
			if (!isSelected)
				continue;
			
			
			BibtexEntry bibtexEntry = (BibtexEntry)r.get("be");
			FileListEntry fileEntry = (FileListEntry)r.get("fe");
			
			File oldFile = Util.expandFilename(
				this.metadata, fileEntry.getLink());
			
			if (oldFile == null)
				continue;
			
			String oldFileName = oldFile.getPath();
			
			String newFileName = null;
			if (mode != 2) {
				String relTargetFileName = (String)r.get(5);
				newFileName = FileNameUtils.joinPaths(
					baseDirectory,
					relTargetFileName);
			}
			
			File newFile = new File(newFileName);

			switch (mode) {
			case 0:
				boolean okay = renameFile(oldFileName, newFileName);
				if (okay) {
					File relativeName = Util.shortenFileName(
						newFile, baseDirectory);
					
					Utils.updateBibtexEntryFile(
						bibtexEntry, fileEntry, relativeName.getPath());
				}
				break;
				
			case 1:
				copyFile(oldFileName, newFileName);
				break;
				
			case 2:
				removeFile(oldFileName);
				break;
			}
		}
	}
	
	private String findAvailableFileName(String newFileName) {
		String result = null;
		int variant = 0;
		for ( ; ; variant++) {
			result = (variant == 0)
				? newFileName
				: FileNameUtils.getFileNameVariant(newFileName, variant);
				
			if (!new File(result).exists())
				break;
		}
		
		if (variant == 0)
			return result;
		
		do {
			result = (String)JOptionPane.showInputDialog(this,
				String.format(
					"File already exists, please choose another name.\n" +
					"Current filename: %s", newFileName),
					"New filename: ",
				JOptionPane.PLAIN_MESSAGE,
				null, null, result);
				
			if (result == null)
				return null;
		} while (new File(result).exists());
		
		return result;
	}
	
	public boolean renameFile(String oldFileName, String newFileName) {
		
		if (FileNameUtils.areSameFiles(oldFileName, newFileName))
			return true;
 	
		Globals.logger("rename: " + oldFileName + " -> " + newFileName);
		
		String targetFileName = findAvailableFileName(newFileName);
		if (targetFileName == null)
			return false;
		
		File sourceFile = new File(oldFileName);
		File targetFile = new File(targetFileName);
		File targetParent = targetFile.getParentFile();
		if (targetParent != null)
			targetParent.mkdirs();
		
		boolean okay = sourceFile.renameTo(targetFile);
		if (okay)
			return true;
		
		JOptionPane.showMessageDialog(
			this,
			String.format(
				"Cannot rename file for whatever reason.\n" +
				"Close all applications using this file, change settings and " +
				"or try with different name.\n" +
				"Source file name: %s\n" +
				"Target file name: %s\n", oldFileName, targetFileName));
		
		return false;
	}

	public boolean copyFile(String oldFileName, String newFileName) {
		String targetFileName = findAvailableFileName(newFileName);
		if (targetFileName == null)
			return false;
		
		File sourceFile = new File(oldFileName);
		File targetFile = new File(targetFileName);
		File targetParent = targetFile.getParentFile();
		if (targetParent != null)
			targetParent.mkdirs();
		
		boolean okay = Utils.copyFile(sourceFile, targetFile);
		if (okay)
			return true;
		
		JOptionPane.showMessageDialog(
			this,
			String.format(
				"Cannot copy file for whatever reason.\n" +
				"Close all applications using this file, change settings and " +
				"or try with different name.\n" +
				"Source file name: %s\n" +
				"Target file name: %s\n", oldFileName, targetFileName));
		
		return false;
	}

	private boolean removeFile(String fileName) {		
		boolean okay = new File(fileName).delete();
		if (okay)
			return true;
		
		JOptionPane.showMessageDialog(
			this,
			String.format(
				"Cannot delete file for whatever reason.\n" +
				"Close all applications using this file, change settings and " +
				"or try with different name.\n" +
				"File name: %s\n", fileName));
		
		return false;
	}

	@Override
	public void focusGained(FocusEvent arg0) {
		// TODO Auto-generated method stub
		
	}	
}
