package org.rvlm.jabref.renamefiles2;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import javax.swing.*;
import net.sf.jabref.*;

// TODO: @christina Remove that line and fix warnings it suppresses.
@SuppressWarnings("serial")
class RenameFilePanel extends SidePaneComponent implements ActionListener {

	private JButton btnMove, btnCopy, btnDelete, btnSettings, btnHelp;
	private JabRefFrame frame;

	public RenameFilePanel(SidePaneManager manager, JabRefFrame frame,
			JMenuItem menu) {
		super(manager, GUIGlobals.getIconUrl("openUrl"), "Rename file");
		this.manager = manager;
		this.frame = frame;
		setActiveBasePanel(frame.basePanel());

		btnMove = getButton("Rename", "Rename/move file(s).", "");
		btnCopy = getButton("Copy", "Copy file(s).", "");
		btnDelete = getButton("Delete", "Detach the local pdf from the "
				+ "BibTeX entry and delete the local pdf from the filesystem.",
				"");
		btnSettings = getButton("", "Settings", "preferences");
		btnHelp = getButton("", "Help", "help");
		JPanel p = new JPanel();
		p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));
		JPanel p1 = new JPanel(new GridBagLayout());
		JPanel p2 = new JPanel(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = .5;
		p1.add(btnMove, c);
		c.gridx = 1;
		p1.add(btnDelete, c);
		c.gridx = 0;
		p2.add(btnCopy, c);
		c.gridx = 1;
		p2.add(btnSettings, c);
		c.gridx = 2;
		p2.add(btnHelp, c);
		p.add(p1);
		p.add(p2);
		setContent(p);
		setName("renamefile");
	}

	private JButton getButton(String title, String tip, String icon) {
		JButton btn = new JButton(GUIGlobals.getImage(icon));
		btn.addActionListener(this);
		btn.setText(title);
		btn.setToolTipText(tip);
		return btn;
	}

	public void actionPerformed(ActionEvent e) {
		MetaData metadata = this.panel.metaData();
		String baseDirectory = Utils.getDatabaseDirectoryForFiles(metadata);
		
		// TODO: @christina Refactor to reduce code duplication.
		if (e.getSource() == btnSettings) {
			new OptionsDialog(frame);
		} else if (e.getSource() == btnMove) {
			BibtexEntry[] bes = panel.mainTable.getSelectedEntries();
			new RenameDialog(frame, bes, metadata, baseDirectory);
		} else if (e.getSource() == btnCopy) {
			BibtexEntry[] bes = panel.mainTable.getSelectedEntries();
			new RenameDialog(frame, bes, metadata, baseDirectory, 1);
		} else if (e.getSource() == btnDelete) {
			BibtexEntry[] bes = panel.mainTable.getSelectedEntries();
			new RenameDialog(frame, bes, metadata, baseDirectory, 2);
		} else if (e.getSource() == btnHelp) {
			displayAbout(frame);
		}
	}

	public void componentOpening() {
		Globals.prefs.putBoolean("renamefileShow", true);
	}

	public void componentClosing() {
		Globals.prefs.putBoolean("renamefileShow", false);
	}

	public static void displayAbout(JFrame parent) {
		InputStream stream = Utils.class.getResourceAsStream("/about.txt");
		String text = streamToString(stream);
		JTextArea ta = new JTextArea(text);
		ta.setEditable(false);
		ta.setBackground(Color.white);
		JScrollPane sp = new JScrollPane(ta);
		sp.setPreferredSize(new Dimension(620, 500));
		JOptionPane.showMessageDialog(parent, sp, "About Renamefile plugin",
				JOptionPane.PLAIN_MESSAGE);
	}

	private static String streamToString(InputStream is) {
		final int len = 0x10000;
		final char[] buffer = new char[len];
		StringBuilder out = new StringBuilder();
		try {
			Reader in = new InputStreamReader(is, "UTF-8");
			int read;
			while ((read = in.read(buffer, 0, len)) != -1)
				out.append(buffer, 0, read);
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return out.toString();
	}
}
